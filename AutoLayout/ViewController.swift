//
//  ViewController.swift
//  AutoLayout
//
//  Created by Darwin Guzmán on 26/6/18.
//  Copyright © 2018 Darwin Guzmán. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var messageTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let message:String? = UserDefaults.standard.value(forKey: "message") as? String
        messageLabel.text = message
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func saveButtonPressed(_ sender: Any) {
        messageLabel.text = messageTextField.text
        UserDefaults.standard.set(messageTextField.text, forKey: "message")
    }
    
}

