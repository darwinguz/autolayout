//
//  TabViewController.swift
//  AutoLayout
//
//  Created by Darwin Guzmán on 27/6/18.
//  Copyright © 2018 Darwin Guzmán. All rights reserved.
//

import UIKit

class TabViewController: UIViewController {
    @IBOutlet weak var mainView: UIView!
    
    @IBAction func tabGestureActivated(_ sender: Any) {
        mainView.backgroundColor = .black
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
