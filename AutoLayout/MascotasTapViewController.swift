//
//  MascotasTapViewController.swift
//  AutoLayout
//
//  Created by Darwin Guzmán on 4/7/18.
//  Copyright © 2018 Darwin Guzmán. All rights reserved.
//

import UIKit

class MascotasTapViewController: UIViewController {

    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var thirdView: UIView!
    @IBOutlet weak var fourthView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func tapFirstActivated(_ sender: Any) {
        firstView.backgroundColor = .green
    }
    
    @IBAction func tapSecondActivated(_ sender: Any) {
        secondView.backgroundColor = .yellow
    }
    
    @IBAction func tapThirdActivated(_ sender: Any) {
        thirdView.backgroundColor = .blue
    }
    
    @IBAction func tapFourthActivated(_ sender: Any) {
        fourthView.backgroundColor = .red
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
